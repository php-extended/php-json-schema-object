<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaArray;
use PhpExtended\JsonSchema\JsonSchemaVisitorNoop;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaArrayTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaArray
 * @internal
 * @small
 */
class JsonSchemaArrayTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaArray
	 */
	protected JsonSchemaArray $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('{"type":"array"}', $this->_object->__toString());
	}
	
	public function testGetItems() : void
	{
		$this->assertNull($this->_object->getItems());
		$this->_object->setItems(new JsonSchema());
		$this->assertEquals(new JsonSchema(), $this->_object->getItems());
		$this->assertEquals(['type' => 'array', 'items' => []], $this->_object->jsonSerialize());
	}
	
	public function testGetPrefixItems() : void
	{
		$this->assertEquals([], $this->_object->getPrefixItems());
		$this->_object->setPrefixItems([new JsonSchema()]);
		$this->assertEquals([new JsonSchema()], $this->_object->getPrefixItems());
		$this->assertEquals(['type' => 'array', 'prefixItems' => [[]]], $this->_object->jsonSerialize());
	}
	
	public function testGetContains() : void
	{
		$this->assertEquals([], $this->_object->getContains());
		$this->_object->setContains([new JsonSchema()]);
		$this->assertEquals([new JsonSchema()], $this->_object->getContains());
		$this->assertEquals(['type' => 'array', 'contains' => [[]]], $this->_object->jsonSerialize());
	}
	
	public function testGetMinContains() : void
	{
		$this->assertNull($this->_object->getMinContains());
		$this->_object->setMinContains(14);
		$this->assertEquals(14, $this->_object->getMinContains());
		$this->assertEquals(['type' => 'array', 'minContains' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetMaxContains() : void
	{
		$this->assertNull($this->_object->getMaxContains());
		$this->_object->setMaxContains(14);
		$this->assertEquals(14, $this->_object->getMaxContains());
		$this->assertEquals(['type' => 'array', 'maxContains' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetMinItems() : void
	{
		$this->assertNull($this->_object->getMinItems());
		$this->_object->setMinItems(14);
		$this->assertEquals(14, $this->_object->getMinItems());
		$this->assertEquals(['type' => 'array', 'minItems' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetMaxItems() : void
	{
		$this->assertNull($this->_object->getMaxItems());
		$this->_object->setMaxItems(14);
		$this->assertEquals(14, $this->_object->getMaxItems());
		$this->assertEquals(['type' => 'array', 'maxItems' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetUniqueItems() : void
	{
		$this->assertNull($this->_object->getUniqueItems());
		$this->_object->setUniqueItems(true);
		$this->assertTrue($this->_object->getUniqueItems());
		$this->assertEquals(['type' => 'array', 'uniqueItems' => true], $this->_object->jsonSerialize());
	}
	
	public function testMergeWithNull() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith(null));
	}
	
	public function testMergeWithSelf() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith($this->_object));
	}
	
	public function testBeVisitedBy() : void
	{
		$this->assertNull($this->_object->beVisitedBy(new JsonSchemaVisitorNoop()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaArray();
	}
	
}
