<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaArray;
use PhpExtended\JsonSchema\JsonSchemaBoolean;
use PhpExtended\JsonSchema\JsonSchemaFloat;
use PhpExtended\JsonSchema\JsonSchemaInteger;
use PhpExtended\JsonSchema\JsonSchemaObject;
use PhpExtended\JsonSchema\JsonSchemaProvider;
use PhpExtended\JsonSchema\JsonSchemaString;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaProviderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaProvider
 * @internal
 * @small
 */
class JsonSchemaProviderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaProvider
	 */
	protected JsonSchemaProvider $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testFailedNonJsonProvided() : void
	{
		$this->expectException(JsonException::class);
		
		$this->_object->provideFromString('<html>');
	}
	
	public function testFailedJsonProvided() : void
	{
		$this->expectException(JsonException::class);
		
		$this->_object->provideFromString('"this is data"');
	}
	
	public function testProvideRef() : void
	{
		$jsonString = '{
	"ref": "#/path/to/ref"
}';
		$this->assertInstanceOf(JsonSchema::class, $this->_object->provideFromString($jsonString));
	}
	
	public function testProvideFile() : void
	{
		$rootSchema = $this->_object->provideFromFile(__DIR__.'/json-schema-test.json');
		$this->assertInstanceOf(JsonSchemaArray::class, $rootSchema);
		$this->assertInstanceOf(JsonSchemaArray::class, $rootSchema->getDefs()['array-typed']);
		$this->assertInstanceOf(JsonSchemaFloat::class, $rootSchema->getDefs()['float-typed']);
		$this->assertInstanceOf(JsonSchemaInteger::class, $rootSchema->getDefs()['integer-typed']);
		$this->assertInstanceOf(JsonSchemaObject::class, $rootSchema->getDefs()['object-typed']);
		$this->assertInstanceOf(JsonSchemaString::class, $rootSchema->getDefs()['string-typed']);
		$this->assertInstanceOf(JsonSchemaBoolean::class, $rootSchema->getDefs()['bool-typed']);
	}
	
	public function testFailedProvideNonExistantFile() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->provideFromFile(__FILE__.'/nonexistant-test-file.json');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaProvider();
	}
	
}
