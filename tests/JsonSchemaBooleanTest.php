<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaBoolean;
use PhpExtended\JsonSchema\JsonSchemaBooleanInterface;
use PhpExtended\JsonSchema\JsonSchemaVisitorNoop;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaBooleanTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaBoolean
 * @internal
 * @small
 */
class JsonSchemaBooleanTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaBoolean
	 */
	protected JsonSchemaBoolean $_object;

	public function testConstructorSetsTypeToBoolean() : void
	{
		$this->assertSame('bool', $this->_object->getType());
	}

	public function testSetDefaultToTrue() : void
	{
		$result = $this->_object->setDefault(true);
		
		$this->assertTrue($this->_object->getDefault());
		$this->assertInstanceOf(JsonSchemaBooleanInterface::class, $result);
		$this->assertSame($this->_object, $result);
	}

	public function testSetExampleToTrue() : void
	{
		$result = $this->_object->setExample(true);
		
		$this->assertTrue($this->_object->getExample());
		$this->assertInstanceOf(JsonSchemaBooleanInterface::class, $result);
		$this->assertSame($this->_object, $result);
	}

	public function testJsonSerializeIncludesBothDefaultAndExampleWhenNotNull() : void
	{
		$this->_object->setDefault(true);
		$this->_object->setExample(false);
		
		$result = $this->_object->jsonSerialize();
		
		$this->assertArrayHasKey('default', $result);
		$this->assertArrayHasKey('example', $result);
		$this->assertTrue($result['default']);
		$this->assertFalse($result['example']);
	}

	public function testMergeWithNullSchemaReturnsNewInstance() : void
	{
		$original = $this->_object;
		$original->setDefault(true);
		$original->setExample(false);
	
		$result = $original->mergeWith(null);
	
		$this->assertInstanceOf(JsonSchemaBoolean::class, $result);
		/** @var JsonSchemaBoolean $result */
		$this->assertNotSame($original, $result);
		$this->assertTrue($result->getDefault());
		$this->assertFalse($result->getExample());
	}

	public function testBeVisitedByCallsVisitSchemaBooleanMethodOfVisitor() : void
	{
		$visitor = new JsonSchemaVisitorNoop();
	
		$result = $this->_object->beVisitedBy($visitor);
	
		$this->assertNull($result);
	}

	public function testMergeWithWhenCurrentSchemaHasNonNullDefaultAndExample() : void
	{
		$original = $this->_object;
		$original->setDefault(true);
		$original->setExample(false);
	
		$newSchema = new JsonSchemaBoolean();
		$newSchema->setDefault(false);
		$newSchema->setExample(true);
	
		$result = $original->mergeWith($newSchema);
	
		$this->assertInstanceOf(JsonSchemaBoolean::class, $result);
		/** @var JsonSchemaBoolean $result */
		$this->assertNotSame($original, $result);
		$this->assertFalse($result->getDefault());
		$this->assertTrue($result->getExample());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaBoolean();
	}
	
}
