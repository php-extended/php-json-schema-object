<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaFloat;
use PhpExtended\JsonSchema\JsonSchemaVisitorNoop;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaFloatTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaFloat
 * @internal
 * @small
 */
class JsonSchemaFloatTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaFloat
	 */
	protected JsonSchemaFloat $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('{"type":"number"}', $this->_object->__toString());
	}

	public function testGetDefault() : void
	{
		$this->assertNull($this->_object->getDefault());
		$this->_object->setDefault(1.0);
		$this->assertEquals(1.0, $this->_object->getDefault());
		$this->assertEquals(['type' => 'number', 'default' => 1.0], $this->_object->jsonSerialize());
	}

	public function testGetExample() : void
	{
		$this->assertNull($this->_object->getExample());
		$this->_object->setExample(2.0);
		$this->assertEquals(2.0, $this->_object->getExample());
		$this->assertEquals(['type' => 'number', 'example' => 2.0], $this->_object->jsonSerialize());
	}
	
	public function testGetMinimum() : void
	{
		$this->assertNull($this->_object->getMinimum());
		$this->_object->setMinimum(14.1);
		$this->assertEquals(14.1, $this->_object->getMinimum());
		$this->assertEquals(['type' => 'number', 'minimum' => 14.1], $this->_object->jsonSerialize());
	}
	
	public function testGetExclusiveMinimum() : void
	{
		$this->assertNull($this->_object->getExclusiveMinimum());
		$this->_object->setExclusiveMinimum(14.1);
		$this->assertEquals(14.1, $this->_object->getExclusiveMinimum());
		$this->assertEquals(['type' => 'number', 'exclusiveMinimum' => 14.1], $this->_object->jsonSerialize());
	}
	
	public function testGetMaximum() : void
	{
		$this->assertNull($this->_object->getMaximum());
		$this->_object->setMaximum(14.1);
		$this->assertEquals(14.1, $this->_object->getMaximum());
		$this->assertEquals(['type' => 'number', 'maximum' => 14.1], $this->_object->jsonSerialize());
	}
	
	public function testGetExclusiveMaximum() : void
	{
		$this->assertNull($this->_object->getExclusiveMaximum());
		$this->_object->setExclusiveMaximum(14.1);
		$this->assertEquals(14.1, $this->_object->getExclusiveMaximum());
		$this->assertEquals(['type' => 'number', 'exclusiveMaximum' => 14.1], $this->_object->jsonSerialize());
	}
	
	public function testMergeWithNull() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith(null));
	}
	
	public function testMergeWithSelf() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith($this->_object));
	}
	
	public function testBeVisitedBy() : void
	{
		$this->assertNull($this->_object->beVisitedBy(new JsonSchemaVisitorNoop()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaFloat();
	}
	
}
