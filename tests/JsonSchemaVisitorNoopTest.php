<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaArray;
use PhpExtended\JsonSchema\JsonSchemaBoolean;
use PhpExtended\JsonSchema\JsonSchemaFloat;
use PhpExtended\JsonSchema\JsonSchemaInteger;
use PhpExtended\JsonSchema\JsonSchemaObject;
use PhpExtended\JsonSchema\JsonSchemaString;
use PhpExtended\JsonSchema\JsonSchemaVisitorNoop;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaVisitorNoopTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaVisitorNoop
 * @internal
 * @small
 */
class JsonSchemaVisitorNoopTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaVisitorNoop
	 */
	protected JsonSchemaVisitorNoop $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitSchemaRaw() : void
	{
		$this->assertNull($this->_object->visit(new JsonSchema()));
	}
	
	public function testVisitSchemaArray() : void
	{
		$this->assertNull($this->_object->visit(new JsonSchemaArray()));
	}

	public function testVisitSchemaBoolean() : void
	{
		$this->assertNull($this->_object->visit(new JsonSchemaBoolean()));
	}
	
	public function testVisitSchemaFloat() : void
	{
		$this->assertNull($this->_object->visit(new JsonSchemaFloat()));
	}
	
	public function testVisitSchemaInteger() : void
	{
		$this->assertNull($this->_object->visit(new JsonSchemaInteger()));
	}
	
	public function testVisitSchemaObject() : void
	{
		$this->assertNull($this->_object->visit(new JsonSchemaObject()));
	}
	
	public function testVisitSchemaString() : void
	{
		$this->assertNull($this->_object->visit(new JsonSchemaString()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaVisitorNoop();
	}
	
}
