<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaVisitorNoop;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchema
 * @internal
 * @small
 */
class JsonSchemaTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchema
	 */
	protected JsonSchema $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[]', $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertNull($this->_object->getId());
		$this->_object->setId(new Uri());
		$this->assertEquals(new Uri(), $this->_object->getId());
		$this->assertEquals(['$id' => '/'], $this->_object->jsonSerialize());
	}
	
	public function testGetSchema() : void
	{
		$this->assertNull($this->_object->getSchema());
		$this->_object->setSchema(new Uri());
		$this->assertEquals(new Uri(), $this->_object->getSchema());
		$this->assertEquals(['$schema' => '/'], $this->_object->jsonSerialize());
	}
	
	public function testGetAnchor() : void
	{
		$this->assertNull($this->_object->getAnchor());
		$this->_object->setAnchor('anchor');
		$this->assertEquals('anchor', $this->_object->getAnchor());
		$this->assertEquals(['anchor' => 'anchor'], $this->_object->jsonSerialize());
	}
	
	public function testGetRef() : void
	{
		$this->assertNull($this->_object->getRef());
		$this->_object->setRef(new Uri());
		$this->assertEquals(new Uri(), $this->_object->getRef());
		$this->assertEquals(['$ref' => '/'], $this->_object->jsonSerialize());
	}
	
	public function testGetType() : void
	{
		$this->assertNull($this->_object->getType());
		$this->_object->setType('type');
		$this->assertEquals('type', $this->_object->getType());
		$this->assertEquals(['type' => 'type'], $this->_object->jsonSerialize());
	}
	
	public function testGetTitle() : void
	{
		$this->assertNull($this->_object->getTitle());
		$this->_object->setTitle('title');
		$this->assertEquals('title', $this->_object->getTitle());
		$this->assertEquals(['title' => 'title'], $this->_object->jsonSerialize());
	}
	
	public function testGetDescription() : void
	{
		$this->assertNull($this->_object->getDescription());
		$this->_object->setDescription('desc');
		$this->assertEquals('desc', $this->_object->getDescription());
		$this->assertEquals(['description' => 'desc'], $this->_object->jsonSerialize());
	}
	
	public function testGetComment() : void
	{
		$this->assertNull($this->_object->getComment());
		$this->_object->setComment('comm');
		$this->assertEquals('comm', $this->_object->getComment());
		$this->assertEquals(['$comment' => 'comm'], $this->_object->jsonSerialize());
	}
	
	public function testGetDeprecated() : void
	{
		$this->assertNull($this->_object->getDeprecated());
		$this->_object->setDeprecated(true);
		$this->assertTrue($this->_object->getDeprecated());
		$this->assertEquals(['deprecated' => true], $this->_object->jsonSerialize());
	}

	public function testGetNullable() : void
	{
		$this->assertNull($this->_object->getNullable());
		$this->_object->setNullable(true);
		$this->assertTrue($this->_object->getNullable());
		$this->assertEquals(['nullable' => true], $this->_object->jsonSerialize());
	}
	
	public function testGetReadOnly() : void
	{
		$this->assertNull($this->_object->getReadOnly());
		$this->_object->setReadOnly(true);
		$this->assertTrue($this->_object->getReadOnly());
		$this->assertEquals(['readOnly' => true], $this->_object->jsonSerialize());
	}
	
	public function testGetWriteOnly() : void
	{
		$this->assertNull($this->_object->getWriteOnly());
		$this->_object->setWriteOnly(true);
		$this->assertTrue($this->_object->getWriteOnly());
		$this->assertEquals(['writeOnly' => true], $this->_object->jsonSerialize());
	}
	
	public function testGetDefs() : void
	{
		$this->assertEquals([], $this->_object->getDefs());
		$this->_object->setDefs([new JsonSchema()]);
		$this->assertEquals([new JsonSchema()], $this->_object->getDefs());
		$this->assertEquals(['$defs' => [[]]], $this->_object->jsonSerialize());
	}
	
	public function testGetFromPathNull() : void
	{
		$this->assertNull($this->_object->getFromPath(null));
	}
	
	public function testGetFromPathEmpty() : void
	{
		$this->assertNull($this->_object->getFromPath(''));
	}
	
	public function testGetFromPathBlank() : void
	{
		$this->assertNull($this->_object->getFromPath('     '));
	}
	
	public function testGetFromPathDiese() : void
	{
		$this->assertEquals($this->_object, $this->_object->getFromPath('#'));
	}
	
	public function testGetFromPathSlash() : void
	{
		$this->assertEquals($this->_object, $this->_object->getFromPath('/'));
	}
	
	public function testGetFromPathInexistantField() : void
	{
		$this->assertNull($this->_object->getFromPath('inexistantField'));
	}
	
	public function testGetFromPathNonSchemaField() : void
	{
		$this->assertNull($this->_object->getFromPath('$id'));
	}
	
	public function testGetFromPathArrayKey() : void
	{
		$schema = new JsonSchema();
		$schema->setRef((new Uri())->withFragment('/ref/to/inner/schema'));
		$this->_object->setDefs(['inner' => $schema]);
		
		$this->assertEquals($schema, $this->_object->getFromPath('#/$defs/inner'));
	}
	
	public function testGetFromPathArrayKeyRecursive() : void
	{
		$schemaInner = new JsonSchema();
		$schemaInner->setRef((new Uri())->withFragment('/ref/to/inner/schema'));
		$schemaOuter = new JsonSchema();
		$schemaOuter->setRef((new Uri())->withFragment('/ref/to/outer/schema'));
		$schemaOuter->setDefs(['inner' => $schemaInner]);
		$this->_object->setDefs(['outer' => $schemaOuter]);
		
		$this->assertEquals($schemaInner, $this->_object->getFromPath('#/$defs/outer/$defs/inner'));
	}
	
	public function testMergeWithNull() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith(null));
	}
	
	public function testMergeWithSelf() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith($this->_object));
	}
	
	public function testBeVisitedBy() : void
	{
		$this->assertNull($this->_object->beVisitedBy(new JsonSchemaVisitorNoop()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchema();
	}
	
}
