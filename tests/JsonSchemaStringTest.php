<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaString;
use PhpExtended\JsonSchema\JsonSchemaVisitorNoop;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaStringTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaString
 * @internal
 * @small
 */
class JsonSchemaStringTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaString
	 */
	protected JsonSchemaString $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('{"type":"string"}', $this->_object->__toString());
	}

	public function testGetDefault() : void
	{
		$this->assertNull($this->_object->getDefault());
		$this->_object->setDefault('test string');
		$this->assertEquals('test string', $this->_object->getDefault());
		$this->assertEquals(['type' => 'string', 'default' => 'test string'], $this->_object->jsonSerialize());
	}

	public function testGetExample() : void
	{
		$this->assertNull($this->_object->getExample());
		$this->_object->setExample('sample text');
		$this->assertEquals('sample text', $this->_object->getExample());
		$this->assertEquals(['type' => 'string', 'example' => 'sample text'], $this->_object->jsonSerialize());
	}
	
	public function testGetMinLength() : void
	{
		$this->assertNull($this->_object->getMinLength());
		$this->_object->setMinLength(12);
		$this->assertEquals(12, $this->_object->getMinLength());
		$this->assertEquals(['type' => 'string', 'minLength' => 12], $this->_object->jsonSerialize());
	}
	
	public function testGetMaxLength() : void
	{
		$this->assertNull($this->_object->getMaxLength());
		$this->_object->setMaxLength(13);
		$this->assertEquals(13, $this->_object->getMaxLength());
		$this->assertEquals(['type' => 'string', 'maxLength' => 13], $this->_object->jsonSerialize());
	}
	
	public function testGetPattern() : void
	{
		$this->assertNull($this->_object->getPattern());
		$this->_object->setPattern('/.*/');
		$this->assertEquals('/.*/', $this->_object->getPattern());
		$this->assertEquals(['type' => 'string', 'pattern' => '/.*/'], $this->_object->jsonSerialize());
	}
	
	public function testGetFormat() : void
	{
		$this->assertNull($this->_object->getFormat());
		$this->_object->setFormat('datetime');
		$this->assertEquals('datetime', $this->_object->getFormat());
		$this->assertEquals(['type' => 'string', 'format' => 'datetime'], $this->_object->jsonSerialize());
	}
	
	public function testMergeWithNull() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith(null));
	}
	
	public function testMergeWithSelf() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith($this->_object));
	}
	
	public function testBeVisitedBy() : void
	{
		$this->assertNull($this->_object->beVisitedBy(new JsonSchemaVisitorNoop()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaString();
	}
	
}
