<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaInteger;
use PhpExtended\JsonSchema\JsonSchemaVisitorNoop;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaIntegerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaInteger
 * @internal
 * @small
 */
class JsonSchemaIntegerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaInteger
	 */
	protected JsonSchemaInteger $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('{"type":"integer"}', $this->_object->__toString());
	}

	public function testGetDefault() : void
	{
		$this->assertNull($this->_object->getDefault());
		$this->_object->setDefault(1);
		$this->assertEquals(1, $this->_object->getDefault());
		$this->assertEquals(['type' => 'integer', 'default' => 1], $this->_object->jsonSerialize());
	}

	public function testGetExample() : void
	{
		$this->assertNull($this->_object->getExample());
		$this->_object->setExample(2);
		$this->assertEquals(2, $this->_object->getExample());
		$this->assertEquals(['type' => 'integer', 'example' => 2], $this->_object->jsonSerialize());
	}
	
	public function testGetMultipleOf() : void
	{
		$this->assertNull($this->_object->getMultipleOf());
		$this->_object->setMultipleOf(14);
		$this->assertEquals(14, $this->_object->getMultipleOf());
		$this->assertEquals(['type' => 'integer', 'multipleOf' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetMinimum() : void
	{
		$this->assertNull($this->_object->getMinimum());
		$this->_object->setMinimum(14);
		$this->assertEquals(14, $this->_object->getMinimum());
		$this->assertEquals(['type' => 'integer', 'minimum' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetExclusiveMinimum() : void
	{
		$this->assertNull($this->_object->getExclusiveMinimum());
		$this->_object->setExclusiveMinimum(14);
		$this->assertEquals(14, $this->_object->getExclusiveMinimum());
		$this->assertEquals(['type' => 'integer', 'exclusiveMinimum' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetMaximum() : void
	{
		$this->assertNull($this->_object->getMaximum());
		$this->_object->setMaximum(14);
		$this->assertEquals(14, $this->_object->getMaximum());
		$this->assertEquals(['type' => 'integer', 'maximum' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetExclusiveMaximum() : void
	{
		$this->assertNull($this->_object->getExclusiveMaximum());
		$this->_object->setExclusiveMaximum(14);
		$this->assertEquals(14, $this->_object->getExclusiveMaximum());
		$this->assertEquals(['type' => 'integer', 'exclusiveMaximum' => 14], $this->_object->jsonSerialize());
	}
	
	public function testMergeWithNull() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith(null));
	}
	
	public function testMergeWithSelf() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith($this->_object));
	}
	
	public function testBeVisitedBy() : void
	{
		$this->assertNull($this->_object->beVisitedBy(new JsonSchemaVisitorNoop()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaInteger();
	}
	
}
