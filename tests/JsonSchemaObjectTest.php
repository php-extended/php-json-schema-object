<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaObject;
use PhpExtended\JsonSchema\JsonSchemaPropertyName;
use PhpExtended\JsonSchema\JsonSchemaVisitorNoop;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaObjectTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchema
 * @covers \PhpExtended\JsonSchema\JsonSchemaObject
 * @internal
 * @small
 */
class JsonSchemaObjectTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaObject
	 */
	protected JsonSchemaObject $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('{"type":"object"}', $this->_object->__toString());
	}
	
	public function testGetProperties() : void
	{
		$this->assertEquals([], $this->_object->getProperties());
		$this->_object->setProperties([new JsonSchema()]);
		$this->assertEquals([new JsonSchema()], $this->_object->getProperties());
		$this->assertEquals(['type' => 'object', 'properties' => [[]]], $this->_object->jsonSerialize());
	}
	
	public function testGetPatternProperties() : void
	{
		$this->assertEquals([], $this->_object->getPatternProperties());
		$this->_object->setPatternProperties([new JsonSchema()]);
		$this->assertEquals([new JsonSchema()], $this->_object->getPatternProperties());
		$this->assertEquals(['type' => 'object', 'patternProperties' => [[]]], $this->_object->jsonSerialize());
	}
	
	public function testGetAdditionalProperties() : void
	{
		$this->assertNull($this->_object->getAdditionalProperties());
		$this->_object->setAdditionalProperties(new JsonSchema());
		$this->assertEquals(new JsonSchema(), $this->_object->getAdditionalProperties());
		$this->assertEquals(['type' => 'object', 'additionalProperties' => []], $this->_object->jsonSerialize());
	}
	
	public function testGetUnevaluatedProperties() : void
	{
		$this->assertNull($this->_object->getUnevaluatedProperties());
		$this->_object->setUnevaluatedProperties(true);
		$this->assertTrue($this->_object->getUnevaluatedProperties());
		$this->assertEquals(['type' => 'object', 'unevaluatedProperties' => true], $this->_object->jsonSerialize());
	}
	
	public function testGetRequired() : void
	{
		$this->assertEquals([], $this->_object->getRequired());
		$this->_object->setRequired(['required']);
		$this->assertEquals(['required'], $this->_object->getRequired());
		$this->assertEquals(['type' => 'object', 'required' => ['required']], $this->_object->jsonSerialize());
	}
	
	public function testGetPropertyNames() : void
	{
		$this->assertNull($this->_object->getPropertyNames());
		$this->_object->setPropertyNames(new JsonSchemaPropertyName('pattern'));
		$this->assertEquals(new JsonSchemaPropertyName('pattern'), $this->_object->getPropertyNames());
		$this->assertEquals(['type' => 'object', 'propertyNames' => ['pattern' => 'pattern']], $this->_object->jsonSerialize());
	}
	
	public function testGetMinProperties() : void
	{
		$this->assertNull($this->_object->getMinProperties());
		$this->_object->setMinProperties(14);
		$this->assertEquals(14, $this->_object->getMinProperties());
		$this->assertEquals(['type' => 'object', 'minProperties' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetMaxProperties() : void
	{
		$this->assertNull($this->_object->getMaxProperties());
		$this->_object->setMaxProperties(14);
		$this->assertEquals(14, $this->_object->getMaxProperties());
		$this->assertEquals(['type' => 'object', 'maxProperties' => 14], $this->_object->jsonSerialize());
	}
	
	public function testGetFromPathDirectObject() : void
	{
		$additionalProperties = new JsonSchema();
		$additionalProperties->setRef((new Uri())->withFragment('/additional/properties'));
		$this->_object->setAdditionalProperties($additionalProperties);
		
		$this->assertEquals($additionalProperties, $this->_object->getFromPath('#/additionalProperties'));
	}
	
	public function testMergeWithNull() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith(null));
	}
	
	public function testMergeWithSelf() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith($this->_object));
	}
	
	public function testBeVisitedBy() : void
	{
		$this->assertNull($this->_object->beVisitedBy(new JsonSchemaVisitorNoop()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaObject();
	}
	
}
