<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaPropertyName;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaPropertyNameTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaPropertyName
 * @internal
 * @small
 */
class JsonSchemaPropertyNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaPropertyName
	 */
	protected JsonSchemaPropertyName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('{"pattern":"#^.*$#"}', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('#^.*$#', $this->_object->getPattern());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaPropertyName('#^.*$#');
	}
	
}
