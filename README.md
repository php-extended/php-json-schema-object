# php-extended/php-json-schema-object

A library that implements the php-extended/php-json-schema-interface interface library.

![coverage](https://gitlab.com/php-extended/php-json-schema-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-json-schema-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-json-schema-object ^8`


## Basic Usage

This library may be used the following way :

```php
use PhpExtended\JsonSchema\JsonSchemaProvider;

$provider = new JsonSchemaProvider();

$schema = $provider->provideFromFile('/path/to/file-schema.json');
// $schema is \PhpExtended\JsonSchema\JsonSchema

```


## License

MIT (See [license file](LICENSE)).
