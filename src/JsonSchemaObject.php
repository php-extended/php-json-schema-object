<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaObject class file.
 * 
 * This is a simple implementation of the JsonSchemaObjectInterface.
 * 
 * @author Anastaszor
 */
class JsonSchemaObject extends JsonSchema implements JsonSchemaObjectInterface
{
	
	/**
	 * The schema for regular properties.
	 * 
	 * @var array<string, JsonSchemaInterface>
	 */
	protected array $_properties = [];
	
	/**
	 * The schema for properties from a specific pattern.
	 * 
	 * @var array<string, JsonSchemaInterface>
	 */
	protected array $_patternProperties = [];
	
	/**
	 * The schema for additional properties.
	 * 
	 * @var ?JsonSchemaInterface
	 */
	protected ?JsonSchemaInterface $_additionalProperties = null;
	
	/**
	 * Whether the validated object is allowed to have unevaluated properties.
	 * 
	 * @var ?boolean
	 */
	protected ?bool $_unevaluatedProperties = null;
	
	/**
	 * The required properties.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_required = [];
	
	/**
	 * The property names of the validated object.
	 * 
	 * @var ?JsonSchemaPropertyNameInterface
	 */
	protected ?JsonSchemaPropertyNameInterface $_propertyNames = null;
	
	/**
	 * The min properties of the validated object.
	 * 
	 * @var ?integer
	 */
	protected ?int $_minProperties = null;
	
	/**
	 * The max properties of the validated object.
	 * 
	 * @var ?integer
	 */
	protected ?int $_maxProperties = null;
	
	/**
	 * Builds a new JsonSchemaObject.
	 */
	public function __construct()
	{
		$this->_type = 'object';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::jsonSerialize()
	 * @psalm-suppress InvalidReturnType
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function jsonSerialize() : array
	{
		$data = (array) parent::jsonSerialize();
		
		foreach($this->_properties as $key => $property)
		{
			/** @phpstan-ignore-next-line */
			$data['properties'][(string) $key] = $property->jsonSerialize();
		}
		
		foreach($this->_patternProperties as $key => $property)
		{
			/** @phpstan-ignore-next-line */
			$data['patternProperties'][(string) $key] = $property->jsonSerialize();
		}
		
		if(null !== $this->_additionalProperties)
		{
			$data['additionalProperties'] = $this->_additionalProperties->jsonSerialize();
		}
		
		if(null !== $this->_unevaluatedProperties)
		{
			$data['unevaluatedProperties'] = $this->_unevaluatedProperties;
		}
		
		if(!empty($this->_required))
		{
			$data['required'] = $this->_required;
		}
		
		if(null !== $this->_propertyNames)
		{
			$data['propertyNames'] = $this->_propertyNames->jsonSerialize();
		}
		
		if(null !== $this->_minProperties)
		{
			$data['minProperties'] = $this->_minProperties;
		}
		
		if(null !== $this->_maxProperties)
		{
			$data['maxProperties'] = $this->_maxProperties;
		}
		
		/** @psalm-suppress InvalidReturnStatement */
		return $data;
	}
	
	/**
	 * Sets the properties for the validated object.
	 * 
	 * @param array<string, JsonSchemaInterface> $properties
	 * @return JsonSchemaObjectInterface
	 */
	public function setProperties(array $properties) : JsonSchemaObjectInterface
	{
		$this->_properties = $properties;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaObjectInterface::getProperties()
	 */
	public function getProperties() : array
	{
		return $this->_properties;
	}
	
	/**
	 * Sets the pattern properties for the validated object.
	 * 
	 * @param array<string, JsonSchemaInterface> $patternProperties
	 * @return JsonSchemaObjectInterface
	 */
	public function setPatternProperties(array $patternProperties) : JsonSchemaObjectInterface
	{
		$this->_patternProperties = $patternProperties;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaObjectInterface::getPatternProperties()
	 */
	public function getPatternProperties() : array
	{
		return $this->_patternProperties;
	}
	
	/**
	 * Sets the type of the additional properties for the validated object.
	 * 
	 * @param ?JsonSchemaInterface $additionalProperties
	 * @return JsonSchemaObjectInterface
	 */
	public function setAdditionalProperties(?JsonSchemaInterface $additionalProperties) : JsonSchemaObjectInterface
	{
		$this->_additionalProperties = $additionalProperties;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaObjectInterface::getAdditionalProperties()
	 */
	public function getAdditionalProperties() : ?JsonSchemaInterface
	{
		return $this->_additionalProperties;
	}
	
	/**
	 * Sets whether the validated object may have unevaluated properties.
	 * 
	 * @param ?boolean $unevaluatedProperties
	 * @return JsonSchemaObjectInterface
	 */
	public function setUnevaluatedProperties(?bool $unevaluatedProperties) : JsonSchemaObjectInterface
	{
		$this->_unevaluatedProperties = $unevaluatedProperties;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaObjectInterface::getUnevaluatedProperties()
	 */
	public function getUnevaluatedProperties() : ?bool
	{
		return $this->_unevaluatedProperties;
	}
	
	/**
	 * Sets the required for the validated object.
	 * 
	 * @param array<integer, string> $required
	 * @return JsonSchemaObjectInterface
	 */
	public function setRequired(array $required) : JsonSchemaObjectInterface
	{
		$this->_required = $required;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaObjectInterface::getRequired()
	 */
	public function getRequired() : array
	{
		return $this->_required;
	}
	
	/**
	 * Sets the property names for the validated object.
	 * 
	 * @param ?JsonSchemaPropertyNameInterface $propertyNames
	 * @return JsonSchemaObjectInterface
	 */
	public function setPropertyNames(?JsonSchemaPropertyNameInterface $propertyNames) : JsonSchemaObjectInterface
	{
		$this->_propertyNames = $propertyNames;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaObjectInterface::getPropertyNames()
	 */
	public function getPropertyNames() : ?JsonSchemaPropertyNameInterface
	{
		return $this->_propertyNames;
	}
	
	/**
	 * Sets the min properties for the validated object.
	 * 
	 * @param ?integer $minProperties
	 * @return JsonSchemaObjectInterface
	 */
	public function setMinProperties(?int $minProperties) : JsonSchemaObjectInterface
	{
		$this->_minProperties = $minProperties;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaObjectInterface::getMinProperties()
	 */
	public function getMinProperties() : ?int
	{
		return $this->_minProperties;
	}
	
	/**
	 * Sets the max properties for the validated object.
	 * 
	 * @param ?integer $maxProperties
	 * @return JsonSchemaObjectInterface
	 */
	public function setMaxProperties(?int $maxProperties) : JsonSchemaObjectInterface
	{
		$this->_maxProperties = $maxProperties;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaObjectInterface::getMaxProperties()
	 */
	public function getMaxProperties() : ?int
	{
		return $this->_maxProperties;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::mergeWith()
	 */
	public function mergeWith(?JsonSchemaInterface $schema = null) : JsonSchemaInterface
	{
		$new = parent::mergeWith($schema);
		
		if($new instanceof JsonSchemaObject && $schema instanceof JsonSchemaObjectInterface)
		{
			$new->setProperties(\array_merge($this->getProperties(), $schema->getProperties()));
			$new->setPatternProperties(\array_merge($this->getPatternProperties(), $schema->getPatternProperties()));
			$thisAdditionalProperties = $this->getAdditionalProperties();
			$new->setAdditionalProperties(null !== $thisAdditionalProperties ? $thisAdditionalProperties->mergeWith($schema->getAdditionalProperties()) : $schema->getAdditionalProperties());
			$new->setUnevaluatedProperties($schema->getUnevaluatedProperties() ?? $this->getUnevaluatedProperties());
			$new->setRequired(\array_merge($schema->getRequired(), $this->getRequired()));
			$new->setPropertyNames($schema->getPropertyNames() ?? $this->getPropertyNames());
			$new->setMinProperties($schema->getMinProperties() ?? $this->getMinProperties());
			$new->setMaxProperties($schema->getMaxProperties() ?? $this->getMaxProperties());
		}
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::beVisitedBy()
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor)
	{
		return $visitor->visitSchemaObject($this);
	}
	
}
