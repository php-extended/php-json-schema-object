<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaString class file.
 * 
 * This is a simple implementation of the JsonSchemaStringInterface.
 * 
 * @author Anastaszor
 */
class JsonSchemaString extends JsonSchema implements JsonSchemaStringInterface
{

	/**
	 * An default value for this schema.
	 * 
	 * @var ?string
	 */
	protected ?string $_default = null;

	/**
	 * An example value for this schema.
	 * 
	 * @var ?string
	 */
	protected ?string $_example = null;
	
	/**
	 * The min length of the string.
	 * 
	 * @var ?integer
	 */
	protected ?int $_minLength = null;
	
	/**
	 * The max length of the string.
	 * 
	 * @var ?integer
	 */
	protected ?int $_maxLength = null;
	
	/**
	 * The pattern of the string.
	 * 
	 * @var ?string
	 */
	protected ?string $_pattern = null;
	
	/**
	 * The format of the string.
	 * 
	 * @var ?string
	 */
	protected ?string $_format = null;
	
	/**
	 * Builds a new JsonSchemaString.
	 */
	public function __construct()
	{
		$this->_type = 'string';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::jsonSerialize()
	 */
	public function jsonSerialize() : array
	{
		$data = parent::jsonSerialize();

		if(null !== $this->_default)
		{
			$data['default'] = $this->_default;
		}

		if(null !== $this->_example)
		{
			$data['example'] = $this->_example;
		}
		
		if(null !== $this->_minLength)
		{
			$data['minLength'] = $this->_minLength;
		}
		
		if(null !== $this->_maxLength)
		{
			$data['maxLength'] = $this->_maxLength;
		}
		
		if(null !== $this->_pattern)
		{
			$data['pattern'] = $this->_pattern;
		}
		
		if(null !== $this->_format)
		{
			$data['format'] = $this->_format;
		}
		
		return $data;
	}

	/**
	 * Gets the default value for this schema.
	 * 
	 * @param ?string $default
	 * @return JsonSchemaStringInterface
	 */
	public function setDefault(?string $default) : JsonSchemaStringInterface
	{
		$this->_default = $default;

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaStringInterface::getDefault()
	 */
	public function getDefault() : ?string
	{
		return $this->_default;
	}

	/**
	 * Gets the example value for this schema.
	 * 
	 * @param ?string $example
	 * @return JsonSchemaStringInterface
	 */
	public function setExample(?string $example) : JsonSchemaStringInterface
	{
		$this->_example = $example;

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaStringInterface::getExample()
	 */
	public function getExample() : ?string
	{
		return $this->_example;
	}
	
	/**
	 * Sets the min length of this string.
	 * 
	 * @param ?integer $minLength
	 * @return JsonSchemaStringInterface
	 */
	public function setMinLength(?int $minLength) : JsonSchemaStringInterface
	{
		$this->_minLength = $minLength;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaStringInterface::getMinLength()
	 */
	public function getMinLength() : ?int
	{
		return $this->_minLength;
	}
	
	/**
	 * Sets the max length of this string.
	 * 
	 * @param ?integer $maxLength
	 * @return JsonSchemaStringInterface
	 */
	public function setMaxLength(?int $maxLength) : JsonSchemaStringInterface
	{
		$this->_maxLength = $maxLength;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaStringInterface::getMaxLength()
	 */
	public function getMaxLength() : ?int
	{
		return $this->_maxLength;
	}
	
	/**
	 * Sets the pattern of this string.
	 * 
	 * @param ?string $pattern
	 * @return JsonSchemaStringInterface
	 */
	public function setPattern(?string $pattern) : JsonSchemaStringInterface
	{
		$this->_pattern = $pattern;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaStringInterface::getPattern()
	 */
	public function getPattern() : ?string
	{
		return $this->_pattern;
	}
	
	/**
	 * Sets the format of this string.
	 * 
	 * @param ?string $format
	 * @return JsonSchemaStringInterface
	 */
	public function setFormat(?string $format) : JsonSchemaStringInterface
	{
		$this->_format = $format;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaStringInterface::getFormat()
	 */
	public function getFormat() : ?string
	{
		return $this->_format;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::mergeWith()
	 */
	public function mergeWith(?JsonSchemaInterface $schema = null) : JsonSchemaInterface
	{
		$new = parent::mergeWith($schema);
		
		if($new instanceof JsonSchemaString && $schema instanceof JsonSchemaStringInterface)
		{
			$new->setDefault($schema->getDefault() ?? $this->getDefault());
			$new->setExample($schema->getExample() ?? $this->getExample());
			$new->setMinLength($schema->getMinLength() ?? $this->getMinLength());
			$new->setMaxLength($schema->getMaxLength() ?? $this->getMaxLength());
			$new->setPattern($schema->getPattern() ?? $this->getPattern());
			$new->setFormat($schema->getFormat() ?? $this->getFormat());
		}
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::beVisitedBy()
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor)
	{
		return $visitor->visitSchemaString($this);
	}
	
}
