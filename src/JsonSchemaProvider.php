<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use InvalidArgumentException;
use JsonException;
use PhpExtended\Ensurer\EnsurerInterface;
use PhpExtended\Ensurer\StrictEnsurer;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PhpExtended\Reifier\ReifierInterface;

/**
 * JsonSchemaProvider class file.
 * 
 * This is a simple implementation of the JsonSchemaProviderInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class JsonSchemaProvider implements JsonSchemaProviderInterface
{
	
	/**
	 * The ensurer.
	 * 
	 * @var ?EnsurerInterface
	 */
	protected ?EnsurerInterface $_ensurer = null;
	
	/**
	 * The reifier.
	 * 
	 * @var ?ReifierInterface
	 */
	protected ?ReifierInterface $_reifier = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaProviderInterface::provideFromFile()
	 */
	public function provideFromFile(string $fileName) : JsonSchemaInterface
	{
		if(!\is_file($fileName))
		{
			throw new InvalidArgumentException('No file found at '.$fileName);
		}
		
		$contents = \file_get_contents($fileName);
		if(false === $contents)
		{
			// should not happen except if the rights are such... not testable
			// @codeCoverageIgnoreStart
			throw new InvalidArgumentException('Failed to get contents from file at '.$fileName);
			// @codeCoverageIgnoreEnd
		}
		
		return $this->provideFromString($contents);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaProviderInterface::provideFromString()
	 * @throws InvalidArgumentException
	 * @throws JsonException
	 * @throws ReificationThrowable
	 */
	public function provideFromString(string $jsonString) : JsonSchemaInterface
	{
		$jsonDecoded = \json_decode($jsonString, true, 512, \JSON_THROW_ON_ERROR);
		
		try
		{
			$jsonData = $this->getEnsurer()->asArray($jsonDecoded);
		}
		catch(InvalidArgumentException $exc)
		{
			throw new JsonException('Unexpected json value, expected array', -1, $exc);
		}
		
		/** @psalm-suppress MixedArgumentTypeCoercion */
		return $this->provideFromArray($jsonData);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaProviderInterface::provideFromArray()
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 */
	public function provideFromArray(array $jsonData) : JsonSchemaInterface
	{
		return $this->getReifier()->reify(JsonSchemaInterface::class, $jsonData);
	}
	
	/**
	 * Gets a suitable configuration for the reifier to be able to reify
	 * json schema structures.
	 * 
	 * @return ReifierConfigurationInterface
	 * @throws InvalidArgumentException
	 */
	public function getReifierConfiguration() : ReifierConfigurationInterface
	{
		$configuration = new ReifierConfiguration();
		
		$configuration->addPolymorphism(JsonSchemaInterface::class, 0, 'type', 'array', JsonSchemaArray::class);
		$configuration->addPolymorphism(JsonSchemaInterface::class, 0, 'type', 'number', JsonSchemaFloat::class);
		$configuration->addPolymorphism(JsonSchemaInterface::class, 0, 'type', 'integer', JsonSchemaInteger::class);
		$configuration->addPolymorphism(JsonSchemaInterface::class, 0, 'type', 'object', JsonSchemaObject::class);
		$configuration->addPolymorphism(JsonSchemaInterface::class, 0, 'type', 'string', JsonSchemaString::class);
		$configuration->addPolymorphism(JsonSchemaInterface::class, 0, 'type', 'boolean', JsonSchemaBoolean::class);
		$configuration->setImplementation(JsonSchemaInterface::class, JsonSchema::class);
		
		$configuration->setIterableInnerType(JsonSchema::class, '$defs', JsonSchemaInterface::class);
		
		$configuration->setIterableInnerType(JsonSchemaArray::class, '$defs', JsonSchemaInterface::class);
		$configuration->setIterableInnerType(JsonSchemaArray::class, 'items', JsonSchemaInterface::class);
		$configuration->setIterableInnerType(JsonSchemaArray::class, 'prefixItems', JsonSchemaInterface::class);
		$configuration->setIterableInnerType(JsonSchemaArray::class, 'contains', JsonSchemaInterface::class);
		
		$configuration->setIterableInnerType(JsonSchemaFloat::class, '$defs', JsonSchemaInterface::class);
		
		$configuration->setIterableInnerType(JsonSchemaInteger::class, '$defs', JsonSchemaInterface::class);
		
		$configuration->setIterableInnerType(JsonSchemaObject::class, '$defs', JsonSchemaInterface::class);
		$configuration->setIterableInnerType(JsonSchemaObject::class, 'properties', JsonSchemaInterface::class);
		$configuration->setIterableInnerType(JsonSchemaObject::class, 'patternProperties', JsonSchemaInterface::class);
		$configuration->setIterableInnerType(JsonSchemaObject::class, 'required', 'string');
		
		$configuration->setIterableInnerType(JsonSchemaString::class, '$defs', JsonSchemaInterface::class);
		
		return $configuration;
	}
	
	/**
	 * Gets the ensurer.
	 * 
	 * @return EnsurerInterface
	 */
	protected function getEnsurer() : EnsurerInterface
	{
		if(null === $this->_ensurer)
		{
			$this->_ensurer = new StrictEnsurer();
		}
		
		return $this->_ensurer;
	}
	
	/**
	 * Gets the reifier.
	 * 
	 * @return ReifierInterface
	 * @throws InvalidArgumentException
	 */
	protected function getReifier() : ReifierInterface
	{
		if(null === $this->_reifier)
		{
			$this->_reifier = new Reifier();
			$this->_reifier->setConfiguration($this->getReifierConfiguration());
		}
		
		return $this->_reifier;
	}
	
}
