<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaBoolean class file.
 * 
 * This is a simple implementation of the JsonSchemaBooleanInterface.
 * 
 * @author Anastaszor
 */
class JsonSchemaBoolean extends JsonSchema implements JsonSchemaBooleanInterface
{

	/**
	 * An default value for this schema.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_default = null;

	/**
	 * An example value for this schema.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_example = null;
	
	/**
	 * Builds a new JsonSchemaBoolean.
	 */
	public function __construct()
	{
		$this->_type = 'bool';
	}

	/**
	 * Gets the default value for this schema.
	 * 
	 * @param ?bool $default
	 * @return JsonSchemaBooleanInterface
	 */
	public function setDefault(?bool $default) : JsonSchemaBooleanInterface
	{
		$this->_default = $default;

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaBooleanInterface::getDefault()
	 */
	public function getDefault() : ?bool
	{
		return $this->_default;
	}

	/**
	 * Gets the example value for this schema.
	 * 
	 * @param ?bool $example
	 * @return JsonSchemaBooleanInterface
	 */
	public function setExample(?bool $example) : JsonSchemaBooleanInterface
	{
		$this->_example = $example;

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaBooleanInterface::getExample()
	 */
	public function getExample() : ?bool
	{
		return $this->_example;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::jsonSerialize()
	 */
	public function jsonSerialize() : array
	{
		$data = parent::jsonSerialize();

		if(null !== $this->_default)
		{
			$data['default'] = $this->_default;
		}

		if(null !== $this->_example)
		{
			$data['example'] = $this->_example;
		}
		
		return $data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::mergeWith()
	 */
	public function mergeWith(?JsonSchemaInterface $schema = null) : JsonSchemaInterface
	{
		$new = parent::mergeWith($schema);
		
		if($new instanceof JsonSchemaBoolean && $schema instanceof JsonSchemaBooleanInterface)
		{
			$new->setDefault($schema->getDefault() ?? $this->getDefault());
			$new->setExample($schema->getExample() ?? $this->getExample());
		}
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::beVisitedBy()
	 * @phpstan-ignore-next-line
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor)
	{
		/** @phpstan-ignore-next-line */
		return $visitor->visitSchemaBoolean($this);
	}
	
}
