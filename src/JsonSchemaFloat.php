<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaFloat class file.
 * 
 * This is a simple implementation of the JsonSchemaFloatInterface.
 * 
 * @author Anastaszor
 */
class JsonSchemaFloat extends JsonSchema implements JsonSchemaFloatInterface
{

	/**
	 * An default value for this schema.
	 * 
	 * @var ?float
	 */
	protected ?float $_default = null;

	/**
	 * An example value for this schema.
	 * 
	 * @var ?float
	 */
	protected ?float $_example = null;
	
	/**
	 * The inclusive minimum.
	 * 
	 * @var ?float
	 */
	protected ?float $_minimum = null;
	
	/**
	 * The exclusive minimum.
	 * 
	 * @var ?float
	 */
	protected ?float $_exclusiveMinimum = null;
	
	/**
	 * The inclusive maximum.
	 * 
	 * @var ?float
	 */
	protected ?float $_maximum = null;
	
	/**
	 * The exclusive maximum.
	 * 
	 * @var ?float
	 */
	protected ?float $_exclusiveMaximum = null;
	
	/**
	 * Builds a new JsonSchemaFloat.
	 */
	public function __construct()
	{
		$this->_type = 'number';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::jsonSerialize()
	 */
	public function jsonSerialize() : array
	{
		$data = parent::jsonSerialize();

		if(null !== $this->_default)
		{
			$data['default'] = $this->_default;
		}

		if(null !== $this->_example)
		{
			$data['example'] = $this->_example;
		}
		
		if(null !== $this->_minimum)
		{
			$data['minimum'] = $this->_minimum;
		}
		if(null !== $this->_maximum)
		{
			$data['maximum'] = $this->_maximum;
		}
		if(null !== $this->_exclusiveMinimum)
		{
			$data['exclusiveMinimum'] = $this->_exclusiveMinimum;
		}
		if(null !== $this->_exclusiveMaximum)
		{
			$data['exclusiveMaximum'] = $this->_exclusiveMaximum;
		}
		
		return $data;
	}

	/**
	 * Gets the default value for this schema.
	 * 
	 * @param ?float $default
	 * @return JsonSchemaFloatInterface
	 */
	public function setDefault(?float $default) : JsonSchemaFloatInterface
	{
		$this->_default = $default;

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFloatInterface::getDefault()
	 */
	public function getDefault() : ?float
	{
		return $this->_default;
	}

	/**
	 * Gets the example value for this schema.
	 * 
	 * @param ?float $example
	 * @return JsonSchemaFloatInterface
	 */
	public function setExample(?float $example) : JsonSchemaFloatInterface
	{
		$this->_example = $example;

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFloatInterface::getExample()
	 */
	public function getExample() : ?float
	{
		return $this->_example;
	}
	
	/**
	 * Sets the inclusive minimum.
	 * 
	 * @param ?float $minimum
	 * @return JsonSchemaFloatInterface
	 */
	public function setMinimum(?float $minimum) : JsonSchemaFloatInterface
	{
		$this->_minimum = $minimum;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFloatInterface::getMinimum()
	 */
	public function getMinimum() : ?float
	{
		return $this->_minimum;
	}
	
	/**
	 * Sets the exclusive minimum.
	 * 
	 * @param ?float $exclusiveMinimum
	 * @return JsonSchemaFloatInterface
	 */
	public function setExclusiveMinimum(?float $exclusiveMinimum) : JsonSchemaFloatInterface
	{
		$this->_exclusiveMinimum = $exclusiveMinimum;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFloatInterface::getExclusiveMinimum()
	 */
	public function getExclusiveMinimum() : ?float
	{
		return $this->_exclusiveMinimum;
	}
	
	/**
	 * Sets the inclusive maximum.
	 * 
	 * @param ?float $maximum
	 * @return JsonSchemaFloatInterface
	 */
	public function setMaximum(?float $maximum) : JsonSchemaFloatInterface
	{
		$this->_maximum = $maximum;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFloatInterface::getMaximum()
	 */
	public function getMaximum() : ?float
	{
		return $this->_maximum;
	}
	
	/**
	 * Sets the exclusive maximum.
	 * 
	 * @param ?float $exclusiveMaximum
	 * @return JsonSchemaFloatInterface
	 */
	public function setExclusiveMaximum(?float $exclusiveMaximum) : JsonSchemaFloatInterface
	{
		$this->_exclusiveMaximum = $exclusiveMaximum;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFloatInterface::getExclusiveMaximum()
	 */
	public function getExclusiveMaximum() : ?float
	{
		return $this->_exclusiveMaximum;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::mergeWith()
	 */
	public function mergeWith(?JsonSchemaInterface $schema = null) : JsonSchemaInterface
	{
		$new = parent::mergeWith($schema);
		
		if($new instanceof JsonSchemaFloat && $schema instanceof JsonSchemaFloatInterface)
		{
			$new->setDefault($schema->getDefault() ?? $this->getDefault());
			$new->setExample($schema->getExample() ?? $this->getExample());
			$new->setMinimum($schema->getMinimum() ?? $this->getMinimum());
			$new->setExclusiveMinimum($schema->getExclusiveMinimum() ?? $this->getExclusiveMinimum());
			$new->setMaximum($schema->getMaximum() ?? $this->getMaximum());
			$new->setExclusiveMaximum($schema->getExclusiveMaximum() ?? $this->getExclusiveMaximum());
		}
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::beVisitedBy()
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor)
	{
		return $visitor->visitSchemaFloat($this);
	}
	
}
