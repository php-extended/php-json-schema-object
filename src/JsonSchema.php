<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Psr\Http\Message\UriInterface;

/**
 * JsonSchema class file.
 * 
 * This is a simple implementation of the JsonSchemaInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class JsonSchema implements JsonSchemaInterface
{
	
	/**
	 * The id of this schema.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_id = null;
	
	/**
	 * The schema specification followed by this schema.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_schema = null;
	
	/**
	 * The anchor of this schema in current document.
	 * 
	 * @var ?string
	 */
	protected ?string $_anchor = null;
	
	/**
	 * The external path of this schema.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_ref = null;
	
	/**
	 * The type of this schema.
	 * 
	 * @var ?string
	 */
	protected ?string $_type = null;
	
	/**
	 * The title of this schema.
	 * 
	 * @var ?string
	 */
	protected ?string $_title = null;
	
	/**
	 * The description of this schema.
	 * 
	 * @var ?string
	 */
	protected ?string $_description = null;
	
	/**
	 * The comment on this schema.
	 * 
	 * @var ?string
	 */
	protected ?string $_comment = null;
	
	/**
	 * Whether this schema is deprecated.
	 * 
	 * @var ?boolean
	 */
	protected ?bool $_deprecated = null;

	/**
	 * Whether this schema data is nullable.
	 * 
	 * @var ?boolean
	 */
	protected ?bool $_nullable = null;
	
	/**
	 * Whether this schema is read only.
	 * 
	 * @var ?boolean
	 */
	protected ?bool $_readOnly = null;
	
	/**
	 * Whether this schema is write only.
	 * 
	 * @var ?boolean
	 */
	protected ?bool $_writeOnly = null;
	
	/**
	 * The definitions.
	 * 
	 * @var array<string, JsonSchemaInterface>
	 */
	protected array $_defs = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return (string) \json_encode($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \JsonSerializable::jsonSerialize()
	 * @return array<string, boolean|integer|float|string|array<string, boolean|integer|float|string>>
	 * @psalm-suppress InvalidReturnType
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function jsonSerialize() : array
	{
		$data = [];
		
		if(null !== $this->_id)
		{
			$data['$id'] = $this->_id->__toString();
		}
		if(null !== $this->_schema)
		{
			$data['$schema'] = $this->_schema->__toString();
		}
		if(null !== $this->_ref)
		{
			$data['$ref'] = $this->_ref->__toString();
		}
		if(null !== $this->_anchor)
		{
			$data['anchor'] = $this->_anchor;
		}
		if(null !== $this->_type)
		{
			$data['type'] = $this->_type;
		}
		if(null !== $this->_title)
		{
			$data['title'] = $this->_title;
		}
		if(null !== $this->_description)
		{
			$data['description'] = $this->_description;
		}
		if(null !== $this->_comment)
		{
			$data['$comment'] = $this->_comment;
		}
		if(null !== $this->_deprecated)
		{
			$data['deprecated'] = $this->_deprecated;
		}
		if(null !== $this->_nullable)
		{
			$data['nullable'] = $this->_nullable;
		}
		if(null !== $this->_readOnly)
		{
			$data['readOnly'] = $this->_readOnly;
		}
		if(null !== $this->_writeOnly)
		{
			$data['writeOnly'] = $this->_writeOnly;
		}
		
		foreach($this->_defs as $def)
		{
			$data['$defs'][] = $def->jsonSerialize();
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return $data;
	}
	
	/**
	 * Sets the id of this schema.
	 * 
	 * @param ?UriInterface $id
	 * @return JsonSchemaInterface
	 */
	public function setId(?UriInterface $id) : JsonSchemaInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getId()
	 */
	public function getId() : ?UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the schema specification url of this schema.
	 * 
	 * @param ?UriInterface $schema
	 * @return JsonSchemaInterface
	 */
	public function setSchema(?UriInterface $schema) : JsonSchemaInterface
	{
		$this->_schema = $schema;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getSchema()
	 */
	public function getSchema() : ?UriInterface
	{
		return $this->_schema;
	}
	
	/**
	 * Sets the anchor of this schema.
	 * 
	 * @param ?string $anchor
	 * @return JsonSchemaInterface
	 */
	public function setAnchor(?string $anchor) : JsonSchemaInterface
	{
		$this->_anchor = $anchor;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getAnchor()
	 */
	public function getAnchor() : ?string
	{
		return $this->_anchor;
	}
	
	/**
	 * Sets the ref of this schema.
	 * 
	 * @param ?UriInterface $ref
	 * @return JsonSchemaInterface
	 */
	public function setRef(?UriInterface $ref) : JsonSchemaInterface
	{
		$this->_ref = $ref;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getRef()
	 */
	public function getRef() : ?UriInterface
	{
		return $this->_ref;
	}
	
	/**
	 * The type of schema.
	 * 
	 * @param ?string $type
	 * @return JsonSchemaInterface
	 */
	public function setType(?string $type) : JsonSchemaInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getType()
	 */
	public function getType() : ?string
	{
		return $this->_type;
	}
	
	/**
	 * Sets the title of this schema.
	 * 
	 * @param ?string $title
	 * @return JsonSchemaInterface
	 */
	public function setTitle(?string $title) : JsonSchemaInterface
	{
		$this->_title = $title;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getTitle()
	 */
	public function getTitle() : ?string
	{
		return $this->_title;
	}
	
	/**
	 * Sets the description of this schema.
	 * 
	 * @param string $description
	 * @return JsonSchemaInterface
	 */
	public function setDescription(?string $description) : JsonSchemaInterface
	{
		$this->_description = $description;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getDescription()
	 */
	public function getDescription() : ?string
	{
		return $this->_description;
	}
	
	/**
	 * Sets the comment of this schema.
	 * 
	 * @param string $comment
	 * @return JsonSchemaInterface
	 */
	public function setComment(?string $comment) : JsonSchemaInterface
	{
		$this->_comment = $comment;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getComment()
	 */
	public function getComment() : ?string
	{
		return $this->_comment;
	}
	
	/**
	 * Sets whether this schema is deprecated.
	 * 
	 * @param ?boolean $deprecated
	 * @return JsonSchemaInterface
	 */
	public function setDeprecated(?bool $deprecated) : JsonSchemaInterface
	{
		$this->_deprecated = $deprecated;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getDeprecated()
	 */
	public function getDeprecated() : ?bool
	{
		return $this->_deprecated;
	}

	/**
	 * Sets whether this schema is nullable.
	 * 
	 * @param ?boolean $nullable
	 * @return JsonSchemaInterface
	 */
	public function setNullable(?bool $nullable) : JsonSchemaInterface
	{
		$this->_nullable = $nullable;
		
		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getNullable()
	 */
	public function getNullable() : ?bool
	{
		return $this->_nullable;
	}
	
	/**
	 * Sets whether this schema is read only.
	 * 
	 * @param ?boolean $readOnly
	 * @return JsonSchemaInterface
	 */
	public function setReadOnly(?bool $readOnly) : JsonSchemaInterface
	{
		$this->_readOnly = $readOnly;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getReadOnly()
	 */
	public function getReadOnly() : ?bool
	{
		return $this->_readOnly;
	}
	
	/**
	 * Sets whether this schema is write only.
	 * 
	 * @param bool $writeOnly
	 * @return JsonSchemaInterface
	 */
	public function setWriteOnly(?bool $writeOnly) : JsonSchemaInterface
	{
		$this->_writeOnly = $writeOnly;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getWriteOnly()
	 */
	public function getWriteOnly() : ?bool
	{
		return $this->_writeOnly;
	}
	
	/**
	 * Sets the defs of this schema.
	 * 
	 * @param array<string, JsonSchemaInterface> $defs
	 * @return JsonSchemaInterface
	 */
	public function setDefs(array $defs) : JsonSchemaInterface
	{
		$this->_defs = $defs;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getDefs()
	 */
	public function getDefs() : array
	{
		return $this->_defs;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::getFromPath()
	 */
	public function getFromPath(?string $path = null) : ?JsonSchemaInterface
	{
		if(null === $path)
		{
			return null;
		}
		
		$path = \trim($path);
		if(empty($path))
		{
			return null;
		}
		
		$path = \trim(\trim($path, '#'));
		$path = \trim(\trim($path, '/'));
		if(empty($path))
		{
			return $this;
		}
		
		$pathParts = \explode('/', $path);
		$jsonPropertyName = \trim($pathParts[0]);
		$phpPropertyName = '_'.\str_replace('$', '', $jsonPropertyName);
		unset($pathParts[0]);
		
		if(\property_exists($this, $phpPropertyName))
		{
			$propertyVal = $this->{$phpPropertyName};
			
			if(\is_object($propertyVal) && $propertyVal instanceof JsonSchemaInterface)
			{
				return $propertyVal->getFromPath('/'.\implode('/', $pathParts));
			}
			
			if(\is_array($propertyVal))
			{
				if(isset($pathParts[1]))
				{
					$jsonKeyName = \trim($pathParts[1]);
					unset($pathParts[1]);
					if(isset($propertyVal[$jsonKeyName]))
					{
						if(\is_object($propertyVal[$jsonKeyName]) && $propertyVal[$jsonKeyName] instanceof JsonSchemaInterface)
						{
							return $propertyVal[$jsonKeyName]->getFromPath('/'.\implode('/', $pathParts));
						}
					}
				}
			}
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::mergeWith()
	 */
	public function mergeWith(?JsonSchemaInterface $schema = null) : JsonSchemaInterface
	{
		if(null === $schema)
		{
			return clone $this;
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress UnsafeInstantiation */
		$new = new static();
		$new->setId($schema->getId() ?? $this->getSchema());
		$new->setSchema($schema->getSchema() ?? $this->getSchema());
		$new->setAnchor($schema->getAnchor() ?? $this->getAnchor());
		$new->setRef($schema->getRef() ?? $this->getRef());
		$new->setType($schema->getType() ?? $this->getType());
		$new->setTitle($schema->getTitle() ?? $this->getTitle());
		$new->setDescription($schema->getDescription() ?? $this->getDescription());
		$new->setComment($schema->getComment() ?? $this->getComment());
		$new->setDeprecated($schema->getDeprecated() ?? $this->getDeprecated());
		$new->setReadOnly($schema->getReadOnly() ?? $this->getReadOnly());
		$new->setWriteOnly($schema->getWriteOnly() ?? $this->getWriteOnly());
		$new->setDefs(\array_merge($schema->getDefs(), $this->getDefs()));
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::beVisitedBy()
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor)
	{
		return $visitor->visitSchemaRaw($this);
	}
	
}
