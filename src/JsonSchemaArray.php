<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaArray class file.
 * 
 * This is a simple implementation of ths JsonSchemaArrayInterface.
 * 
 * @author Anastaszor
 */
class JsonSchemaArray extends JsonSchema implements JsonSchemaArrayInterface
{
	
	/**
	 * The types of the regular items can use to be in the array.
	 * 
	 * @var JsonSchemaInterface
	 */
	protected ?JsonSchemaInterface $_items = null;
	
	/**
	 * The types for which indexes matter.
	 * 
	 * @var array<integer, JsonSchemaInterface>
	 */
	protected array $_prefixItems = [];
	
	/**
	 * The types the validated array must have at least once.
	 * 
	 * @var array<integer, JsonSchemaInterface>
	 */
	protected array $_contains = [];
	
	/**
	 * The min number of contains items in the validated array.
	 * 
	 * @var ?integer
	 */
	protected ?int $_minContains = null;
	
	/**
	 * The max number of contains items in the validated array.
	 * 
	 * @var ?integer
	 */
	protected ?int $_maxContains = null;
	
	/**
	 * The min number of items in the validated array.
	 * 
	 * @var ?integer
	 */
	protected ?int $_minItems = null;
	
	/**
	 * The max number of items in the validated array.
	 * 
	 * @var ?integer
	 */
	protected ?int $_maxItems = null;
	
	/**
	 * Whether all items must be unique.
	 * 
	 * @var ?boolean
	 */
	protected ?bool $_uniqueItems = null;
	
	/**
	 * Builds a new JsonSchemaArray.
	 */
	public function __construct()
	{
		$this->_type = 'array';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::jsonSerialize()
	 * @psalm-suppress InvalidReturnType
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function jsonSerialize() : array
	{
		$data = (array) parent::jsonSerialize();
		
		if(null !== $this->_items)
		{
			$data['items'] = $this->_items->jsonSerialize();
		}
		
		foreach($this->_prefixItems as $item)
		{
			$data['prefixItems'][] = $item->jsonSerialize();
		}
		
		foreach($this->_contains as $item)
		{
			$data['contains'][] = $item->jsonSerialize();
		}
		
		if(null !== $this->_minContains)
		{
			$data['minContains'] = $this->_minContains;
		}
		if(null !== $this->_maxContains)
		{
			$data['maxContains'] = $this->_maxContains;
		}
		if(null !== $this->_minItems)
		{
			$data['minItems'] = $this->_minItems;
		}
		if(null !== $this->_maxItems)
		{
			$data['maxItems'] = $this->_maxItems;
		}
		if(null !== $this->_uniqueItems)
		{
			$data['uniqueItems'] = $this->_uniqueItems;
		}
		
		/** @psalm-suppress InvalidReturnStatement */
		return $data;
	}
	
	/**
	 * Sets the types the regular items may be of.
	 * 
	 * @param ?JsonSchemaInterface $items
	 * @return JsonSchemaArrayInterface
	 */
	public function setItems(?JsonSchemaInterface $items) : JsonSchemaArrayInterface
	{
		$this->_items = $items;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaArrayInterface::getItems()
	 */
	public function getItems() : ?JsonSchemaInterface
	{
		return $this->_items;
	}
	
	/**
	 * Sets the prefix-ed items for the validated array.
	 * 
	 * @param array<integer, JsonSchemaInterface> $prefixItems
	 * @return JsonSchemaArrayInterface
	 */
	public function setPrefixItems(array $prefixItems) : JsonSchemaArrayInterface
	{
		$this->_prefixItems = $prefixItems;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaArrayInterface::getPrefixItems()
	 */
	public function getPrefixItems() : array
	{
		return $this->_prefixItems;
	}
	
	/**
	 * Sets the contains types for the validated array.
	 * 
	 * @param array<integer, JsonSchemaInterface> $contains
	 * @return JsonSchemaArrayInterface
	 */
	public function setContains(array $contains) : JsonSchemaArrayInterface
	{
		$this->_contains = $contains;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaArrayInterface::getContains()
	 */
	public function getContains() : array
	{
		return $this->_contains;
	}
	
	/**
	 * Sets the min number of items for the contains.
	 * 
	 * @param ?integer $minContains
	 * @return JsonSchemaArrayInterface
	 */
	public function setMinContains(?int $minContains) : JsonSchemaArrayInterface
	{
		$this->_minContains = $minContains;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaArrayInterface::getMinContains()
	 */
	public function getMinContains() : ?int
	{
		return $this->_minContains;
	}
	
	/**
	 * Sets the max number of items for the contains.
	 * 
	 * @param ?integer $maxContains
	 * @return JsonSchemaArrayInterface
	 */
	public function setMaxContains(?int $maxContains) : JsonSchemaArrayInterface
	{
		$this->_maxContains = $maxContains;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaArrayInterface::getMaxContains()
	 */
	public function getMaxContains() : ?int
	{
		return $this->_maxContains;
	}
	
	/**
	 * Sets the min number of items in the validated array.
	 * 
	 * @param ?integer $minItems
	 * @return JsonSchemaArrayInterface
	 */
	public function setMinItems(?int $minItems) : JsonSchemaArrayInterface
	{
		$this->_minItems = $minItems;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaArrayInterface::getMinItems()
	 */
	public function getMinItems() : ?int
	{
		return $this->_minItems;
	}
	
	/**
	 * Sets the max number of items in the validated array.
	 * 
	 * @param ?integer $maxItems
	 * @return JsonSchemaArrayInterface
	 */
	public function setMaxItems(?int $maxItems) : JsonSchemaArrayInterface
	{
		$this->_maxItems = $maxItems;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaArrayInterface::getMaxItems()
	 */
	public function getMaxItems() : ?int
	{
		return $this->_maxItems;
	}
	
	/**
	 * Sets whether this array has unique items.
	 * 
	 * @param ?boolean $uniqueItems
	 * @return JsonSchemaArrayInterface
	 */
	public function setUniqueItems(?bool $uniqueItems) : JsonSchemaArrayInterface
	{
		$this->_uniqueItems = $uniqueItems;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaArrayInterface::getUniqueItems()
	 */
	public function getUniqueItems() : ?bool
	{
		return $this->_uniqueItems;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::mergeWith()
	 */
	public function mergeWith(?JsonSchemaInterface $schema = null) : JsonSchemaInterface
	{
		$new = parent::mergeWith($schema);
		
		if($new instanceof JsonSchemaArray && $schema instanceof JsonSchemaArrayInterface)
		{
			$new->setItems($schema->getItems() ?? $this->getItems());
			$new->setPrefixItems(\array_merge($this->getPrefixItems(), $schema->getPrefixItems()));
			$new->setContains(\array_merge($this->getContains(), $schema->getContains()));
			$new->setMinContains($schema->getMinContains() ?? $this->getMinContains());
			$new->setMaxContains($schema->getMaxContains() ?? $this->getMaxContains());
			$new->setMinItems($schema->getMinItems() ?? $this->getMinItems());
			$new->setMaxItems($schema->getMaxItems() ?? $this->getMaxItems());
			$new->setUniqueItems($schema->getUniqueItems() ?? $this->getUniqueItems());
		}
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::beVisitedBy()
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor)
	{
		return $visitor->visitSchemaArray($this);
	}
	
}
