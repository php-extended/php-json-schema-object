<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaPropertyName class file.
 * 
 * This is a simple implementation of the JsonSchemaPropertynameInterface.
 * 
 * @author Anastaszor
 */
class JsonSchemaPropertyName implements JsonSchemaPropertyNameInterface
{
	
	/**
	 * The pattern for regex matching.
	 * 
	 * @var string
	 */
	protected string $_pattern;
	
	/**
	 * Builds a new JsonSchemaPropertyName with its required data.
	 * 
	 * @param string $pattern
	 */
	public function __construct(string $pattern)
	{
		$this->_pattern = $pattern;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return (string) \json_encode($this->jsonSerialize());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \JsonSerializable::jsonSerialize()
	 * @return array<string, string>
	 */
	public function jsonSerialize() : array
	{
		return ['pattern' => $this->_pattern];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaPropertyNameInterface::getPattern()
	 */
	public function getPattern() : string
	{
		return $this->_pattern;
	}
	
}
