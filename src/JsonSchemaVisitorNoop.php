<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaVisitorNoop class file.
 * 
 * This is a visitor that does nothing.
 * 
 * @author Anastaszor
 * @template T of null|integer|float|string|array|object
 * @implements JsonSchemaVisitorInterface<T>
 */
class JsonSchemaVisitorNoop implements JsonSchemaVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorInterface::visit()
	 */
	public function visit(JsonSchemaInterface $schema)
	{
		return $schema->beVisitedBy($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorInterface::visitSchemaRaw()
	 */
	public function visitSchemaRaw(JsonSchemaInterface $schema)
	{
		/** @phpstan-ignore-next-line */
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorInterface::visitSchemaArray()
	 */
	public function visitSchemaArray(JsonSchemaArrayInterface $schema)
	{
		/** @phpstan-ignore-next-line */
		return null;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorInterface::visitSchemaBoolean()
	 */
	public function visitSchemaBoolean(JsonSchemaBooleanInterface $schema)
	{
		/** @phpstan-ignore-next-line */
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorInterface::visitSchemaFloat()
	 */
	public function visitSchemaFloat(JsonSchemaFloatInterface $schema)
	{
		/** @phpstan-ignore-next-line */
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorInterface::visitSchemaInteger()
	 */
	public function visitSchemaInteger(JsonSchemaIntegerInterface $schema)
	{
		/** @phpstan-ignore-next-line */
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorInterface::visitSchemaObject()
	 */
	public function visitSchemaObject(JsonSchemaObjectInterface $schema)
	{
		/** @phpstan-ignore-next-line */
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorInterface::visitSchemaString()
	 */
	public function visitSchemaString(JsonSchemaStringInterface $schema)
	{
		/** @phpstan-ignore-next-line */
		return null;
	}
	
}
