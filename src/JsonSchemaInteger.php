<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaInteger class file.
 * 
 * This is a simple implementation of the JsonSchemaIntegerInterface.
 * 
 * @author Anastaszor
 */
class JsonSchemaInteger extends JsonSchema implements JsonSchemaIntegerInterface
{

	/**
	 * An default value for this schema.
	 * 
	 * @var ?int
	 */
	protected ?int $_default = null;

	/**
	 * An example value for this schema.
	 * 
	 * @var ?int
	 */
	protected ?int $_example = null;
	
	/**
	 * The multiple of this integer must be.
	 * 
	 * @var ?integer
	 */
	protected ?int $_multipleOf = null;
	
	/**
	 * The inclusive minimum.
	 * 
	 * @var ?integer
	 */
	protected ?int $_minimum = null;
	
	/**
	 * The exclusive minimum.
	 * 
	 * @var ?integer
	 */
	protected ?int $_exclusiveMinimum = null;
	
	/**
	 * The inclusive maximum.
	 * 
	 * @var ?integer
	 */
	protected ?int $_maximum = null;
	
	/**
	 * The exclusive maximum.
	 * 
	 * @var ?integer
	 */
	protected ?int $_exclusiveMaximum = null;
	
	/**
	 * Builds a new JsonSchemaInteger.
	 */
	public function __construct()
	{
		$this->_type = 'integer';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::jsonSerialize()
	 */
	public function jsonSerialize() : array
	{
		$data = parent::jsonSerialize();

		if(null !== $this->_default)
		{
			$data['default'] = $this->_default;
		}

		if(null !== $this->_example)
		{
			$data['example'] = $this->_example;
		}
		
		if(null !== $this->_multipleOf)
		{
			$data['multipleOf'] = $this->_multipleOf;
		}
		if(null !== $this->_minimum)
		{
			$data['minimum'] = $this->_minimum;
		}
		if(null !== $this->_maximum)
		{
			$data['maximum'] = $this->_maximum;
		}
		if(null !== $this->_exclusiveMinimum)
		{
			$data['exclusiveMinimum'] = $this->_exclusiveMinimum;
		}
		if(null !== $this->_exclusiveMaximum)
		{
			$data['exclusiveMaximum'] = $this->_exclusiveMaximum;
		}
		
		return $data;
	}

	/**
	 * Gets the default value for this schema.
	 * 
	 * @param ?int $default
	 * @return JsonSchemaIntegerInterface
	 */
	public function setDefault(?int $default) : JsonSchemaIntegerInterface
	{
		$this->_default = $default;

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaIntegerInterface::getDefault()
	 */
	public function getDefault() : ?int
	{
		return $this->_default;
	}

	/**
	 * Gets the example value for this schema.
	 * 
	 * @param ?int $example
	 * @return JsonSchemaIntegerInterface
	 */
	public function setExample(?int $example) : JsonSchemaIntegerInterface
	{
		$this->_example = $example;

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaIntegerInterface::getExample()
	 */
	public function getExample() : ?int
	{
		return $this->_example;
	}
	
	/**
	 * Sets the multiple of this integer.
	 * 
	 * @param ?integer $multipleOf
	 * @return JsonSchemaIntegerInterface
	 */
	public function setMultipleOf(?int $multipleOf) : JsonSchemaIntegerInterface
	{
		$this->_multipleOf = $multipleOf;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaIntegerInterface::getMultipleOf()
	 */
	public function getMultipleOf() : ?int
	{
		return $this->_multipleOf;
	}
	
	/**
	 * Sets the inclusive minimum.
	 * 
	 * @param ?integer $minimum
	 * @return JsonSchemaIntegerInterface
	 */
	public function setMinimum(?int $minimum) : JsonSchemaIntegerInterface
	{
		$this->_minimum = $minimum;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaIntegerInterface::getMinimum()
	 */
	public function getMinimum() : ?int
	{
		return $this->_minimum;
	}
	
	/**
	 * Sets the exclusive minimum.
	 * 
	 * @param ?integer $exclusiveMinimum
	 * @return JsonSchemaIntegerInterface
	 */
	public function setExclusiveMinimum(?int $exclusiveMinimum) : JsonSchemaIntegerInterface
	{
		$this->_exclusiveMinimum = $exclusiveMinimum;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaIntegerInterface::getExclusiveMinimum()
	 */
	public function getExclusiveMinimum() : ?int
	{
		return $this->_exclusiveMinimum;
	}
	
	/**
	 * Sets the inclusive maximum.
	 * 
	 * @param ?integer $maximum
	 * @return JsonSchemaIntegerInterface
	 */
	public function setMaximum(?int $maximum) : JsonSchemaIntegerInterface
	{
		$this->_maximum = $maximum;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaIntegerInterface::getMaximum()
	 */
	public function getMaximum() : ?int
	{
		return $this->_maximum;
	}
	
	/**
	 * Sets the exclusive maximum.
	 * 
	 * @param ?integer $exclusiveMaximum
	 * @return JsonSchemaIntegerInterface
	 */
	public function setExclusiveMaximum(?int $exclusiveMaximum) : JsonSchemaIntegerInterface
	{
		$this->_exclusiveMaximum = $exclusiveMaximum;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaIntegerInterface::getExclusiveMaximum()
	 */
	public function getExclusiveMaximum() : ?int
	{
		return $this->_exclusiveMaximum;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchema::mergeWith()
	 */
	public function mergeWith(?JsonSchemaInterface $schema = null) : JsonSchemaInterface
	{
		$new = parent::mergeWith($schema);
		
		if($new instanceof JsonSchemaInteger && $schema instanceof JsonSchemaIntegerInterface)
		{
			$new->setDefault($schema->getDefault() ?? $this->getDefault());
			$new->setExample($schema->getExample() ?? $this->getExample());
			$new->setMultipleOf($schema->getMultipleOf() ?? $this->getMultipleOf());
			$new->setMinimum($schema->getMinimum() ?? $this->getMinimum());
			$new->setExclusiveMinimum($schema->getExclusiveMinimum() ?? $this->getExclusiveMinimum());
			$new->setMaximum($schema->getMaximum() ?? $this->getMaximum());
			$new->setExclusiveMaximum($schema->getExclusiveMaximum() ?? $this->getExclusiveMaximum());
		}
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaInterface::beVisitedBy()
	 */
	public function beVisitedBy(JsonSchemaVisitorInterface $visitor)
	{
		return $visitor->visitSchemaInteger($this);
	}
	
}
