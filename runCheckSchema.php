<?php

use PhpExtended\JsonSchema\JsonSchemaProvider;

/**
 * Test script for JsonSchemaProvider
 * 
 * Usage:
 * php runCheckSchema.php "/path/to/file.json"
 * 
 * @author Anastaszor
 */

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the path to the file to check.');
}
$filePath = $argv[1];

echo "\n\tInput : ".$filePath."\n\n";

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first');
}
require $composer;

$provider = new JsonSchemaProvider();

var_dump($provider->provideFromFile($filePath));
